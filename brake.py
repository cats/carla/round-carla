import math

gravity = 9.80665                               # m/s^2
rho = 1.225                                     # kg/m^3
TeslaM3_mass = 1845                             # kg
TeslaM3_frontal_area = 2.22                     # m^2
TeslaM3_drag_coeff = 0.30                       # dimensionless
TeslaM3_tyre_friction = 3.5                     # dimensionless
TeslaM3_tyre_radius = 0.37                      # m
TeslaM3_max_brake_torque_per_wheel = 1500       # N.m
max_deceleration_for_longitudinal_comfort = 3.4 # m/s^2
max_brake_time = 10                             # s
p = float                                       # bar - tyre pressure

k = 0.5 * rho * TeslaM3_frontal_area * TeslaM3_drag_coeff

max_brakeForce = TeslaM3_mass * max_deceleration_for_longitudinal_comfort   # F = ma

max_tyre_force_for_no_sliding = TeslaM3_tyre_friction * TeslaM3_mass * gravity


def adv_brake_ctrl(v_0, v_f, tyre_pressure=3, dt=0.1):
    # V_0 and V_f in km/h
    brakeInput, estAcceleration, estVelocity, estDistance = [], [], [], []
    global TeslaM3_mass, p
    p = tyre_pressure

    def aeroForce(v):
    # v is the velocity in km/h

        f_a = k * (v/3.6)**2
        return f_a

    def rollingForce(v):
        # v is the velocity in km/h
        # p is the tire pressure in bar
        # rrc is the rolling_resistance_coefficient (force = rrc * weight)

        global TeslaM3_mass, gravity, p

        rrc = 0.005 + (1/p) * (0.01 + 0.0095 * ((v/100)**2))
        f_r = rrc * TeslaM3_mass * gravity
        return f_r

    def brakeFroce(brake):
        # 0 < brake <= 1

        global TeslaM3_max_brake_torque_per_wheel, max_brakeForce, TeslaM3_tyre_radius, max_tyre_force_for_no_sliding

        f_b = 4 * brake * TeslaM3_max_brake_torque_per_wheel / TeslaM3_tyre_radius
        if f_b > max_tyre_force_for_no_sliding:
            f_b = max_tyre_force_for_no_sliding
            print("WARNING: The tyres are prone to sliding")

        return f_b

    def totalForce(v, brake):
        f_total = aeroForce(v) + rollingForce(v) + brakeFroce(brake)
        return f_total



    print()
    print(f"Max deceleration of {round(totalForce(40,0.375)/TeslaM3_mass,6)} happens with 0.375 brake at speed of 40 km/h")
    print()
    print("The brake should be saturated at 0.375 due to passenger comfort")
    print()
    # v = 40km/h and brake = 0.375 results an acceleration of -3.4 m/s^2

    taw_brake = 1 / (math.log(20))  # 95% response at 1 sec
    brake_list = [round(-0.375 * math.exp(-i*dt/taw_brake) + 0.375,4) for i in range(round(max_brake_time/dt))]

    
    class vehicle_brake_ctrl(object):
    
        def __init__(self,v_0,v_f):     # both in km/h
            self.ddx = 0
            self.dx = v_0 / 3.6
            self.x = 0
            self.dxLast = self.dx
            self.ddxLast = self.ddx
            self.target_vel = v_f
            self.estAcc = []
            self.estVel = []
            self.estDist = []

        def set_ddx(self, _v, _brake):      # km/h, 0 < brake <=1
            self.ddx = - totalForce(_v, _brake) / TeslaM3_mass

        def get_ddx(self):
            return self.ddx

        def set_dx(self):
            self.dxLast = self.dx
            self.dx += 0.5 * (self.ddxLast + self.ddx) * dt   # trapezoidal approximation

        def get_dx(self):
            return self.dx

        def set_x(self):
            self.x += 0.5 * (self.dxLast + self.dx) * dt   # trapezoidal approximation

        def get_x(self):
            return self.x
        
        def compute(self):
            for brake in brake_list:

                v = self.get_dx()*3.6
                self.set_ddx(v, brake)
                self.set_dx()
                self.set_x()
                _acc = round(self.get_ddx(), 4)
                _vel = round(self.get_dx(), 4)
                _dist = round(self.get_x(), 4)
                v = self.get_dx()*3.6
                if v > self.target_vel:
                    self.estAcc.append(_acc)
                    self.estVel.append(_vel)
                    self.estDist.append(_dist)
                else:
                    break

    insight = vehicle_brake_ctrl(v_0,v_f)
    insight.compute()

    estDistance = insight.estDist
    estVelocity = insight.estVel
    estAcceleration = insight.estAcc
    brakeInput = brake_list[0:len(estAcceleration)]

    required_brakeTime = len(brakeInput) * dt
    required_brakeDistance = estDistance[-1]


    return [brakeInput, estAcceleration, estVelocity, estDistance, required_brakeTime, required_brakeDistance]
