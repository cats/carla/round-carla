# RoundD-CARLA

RounD-CARLA is a scenario and control instructions to replay the [RounD dataset](https://www.round-dataset.com/) in the [CARLA simulator](https://carla.org/).


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Installation Instructions](#installation-instructions)
* [Demonstration](#demonstration)
* [Documentation](#documentation)
* [Authors and acknowledgement](#authors-and-acknowledgement)
* [License](#license)

## General info
This project aims to present the technical part of simulation of Real-world dataset [RounD](https://www.round-dataset.com/) dataset in [CARLA simulator](https://carla.org/).
	
## Technologies
Project is created with:
* CARLA simulator version: 0.9.10 (or above)
* Python version: 3.7
* RoadRunner R2022a

## Installation instructions	

To run this project, download the main repository into your device..

### Note
This code in this repository requires a highly computation power, it spawns and controls almost 250 vehicles in the Map with high frame rate 25 fps.
Tested on Ubuntu 20.04.4 LTS (focal) with CARLA 0.9.10 with high-level computational capacity (16 CPUs). We cannot garanty a realistic scenario by using less capacities, thus to ensure that it runs properly in you PC, you need to have such similar capabilities. 

### Setup
```bash
## Launch CARLA server
$ ./CarlaUE4.sh
## Wait few seconds for Map to be loaded
## Run the entrypoint script
$ cd CARLA_ROUND
$ python3.7  scenario_loader.py
## the loader loads and synchronises the 4 controllers according to the 4 legs of roundabout
## It will run:
## Top-Left controller;     python3.7 vehicles_side_controller.py --side tl 
## Top-Right controller;    python3.7 vehicles_side_controller.py --side tr
## Bottom-Left controller;  python3.7 vehicles_side_controller.py --side bl
## Bottom-Right controller; python3.7 vehicles_side_controller.py --side br    
```

## Demonstration

![Alt Text](https://nextcloud.eurecom.fr/s/cAsnx5iQ4QfYbAx/download/Round-Carla.gif)


For full demonstration, please check the full demo (12 minutes) in the repository [here](videos).

## Documentation
Please read [CARLA documentation](https://carla.readthedocs.io/en/latest/) for details related to this scenario.


## Authors and acknowledgement
This scenario has been developped by [EURECOM](https://www.eurecom.fr) and is maintained by Ali.Nadar. 

## License
The scenario is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.

## Citation
Please cite the following paper when using the RounD-CARLA scenario

A. Nadar, M. Lafon, J. Härri, "**A RounD-like Roundabout Scenario in CARLA Simulator**", in _Proc. of the conference on Management of Future Motorway and Urban Traffic Systems (MFTS 2022)_, November 30-December 2nd 2022, Desden Germany. [paper](MFTS2022.pdf) - [slides](MFTS2022-slides.pdf)
