'''
* Copyright 2021-2022 EURECOM
* Author - Mouradi Sina (mouradi@eurecom.fr)
* Date created - 10/06/2022
'''

Vel_1 = 3.6*3.32  # The s.s. velocity(m/s) that corrresponds to 30% throttle
Vel_2 = 3.6*4.24  # The s.s. velocity(m/s) that corrresponds to 35% throttle
Vel_3 = 3.6*5.36  # The s.s. velocity(m/s) that corrresponds to 40% throttle
Vel_4 = 3.6*6.74  # The s.s. velocity(m/s) that corrresponds to 45% throttle
Vel_5 = 3.6*8.12  # The s.s. velocity(m/s) that corrresponds to 50% throttle
Vel_6 = 3.6*9.52  # The s.s. velocity(m/s) that corrresponds to 55% throttle
Vel_7 = 3.6*11.1  # The s.s. velocity(m/s) that corrresponds to 60% throttle
Vel_8 = 3.6*13.01  # The s.s. velocity(m/s) that corrresponds to 65% throttle
Vel_9 = 3.6*15.18  # The s.s. velocity(m/s) that corrresponds to 70% throttle
Vel_10 = 3.6*17.73  # The s.s. velocity(m/s) that corrresponds to 75% throttle

thr_1 = 0.30
thr_2 = 0.35
thr_3 = 0.40
thr_4 = 0.45
thr_5 = 0.50
thr_6 = 0.55
thr_7 = 0.60
thr_8 = 0.65
thr_9 = 0.70
thr_10 = 0.75

AA1 = -0.4513
BB1 = 5.0227
AA2 = -0.4294
BB2 = 5.1997
AA3 = -0.3546
BB3 = 4.7518
AA4 = -0.3205
BB4 = 4.8013
AA5 = -0.3356
BB5 = 5.4463
AA6 = -0.3401
BB6 = 5.8776
AA7 = -0.3367
BB7 = 6.2054
AA8 = -0.2876
BB8 = 5.7554
AA9 = -0.2560
BB9 = 5.5513
AA10 = -0.2366
BB10 = 5.5938



def transfer_function_interpolate(velocity):
    global Vel_1, Vel_2, Vel_3, Vel_4, Vel_5, Vel_6, Vel_7, Vel_8, Vel_9, Vel_10
    global k_1, k_2, k_3, k_4, k_5, k_6, k_7
    global taw_1, taw_2, taw_3, taw_4, taw_5, taw_6, taw_7

    if velocity < Vel_1:
        thr = thr_1
        AA = AA1
        BB = BB1


    elif Vel_1 <= velocity < Vel_2:
        thr = thr_1 + ((velocity - Vel_1) / (Vel_2 - Vel_1)) * (thr_2 - thr_1)
        AA = AA1 + ((velocity - Vel_1) / (Vel_2 - Vel_1)) * (AA2 - AA1)
        BB = BB1 + ((velocity - Vel_1) / (Vel_2 - Vel_1)) * (BB2 - BB1)


    elif Vel_2 <= velocity < Vel_3:
        thr = thr_2 + ((velocity - Vel_2) / (Vel_3 - Vel_2)) * (thr_3 - thr_2)
        AA = AA2 + ((velocity - Vel_2) / (Vel_3 - Vel_2)) * (AA3 - AA2)
        BB = BB2 + ((velocity - Vel_2) / (Vel_3 - Vel_2)) * (BB3 - BB2)


    elif Vel_3 <= velocity < Vel_4:
        thr = thr_3 + ((velocity - Vel_3) / (Vel_4 - Vel_3)) * (thr_4 - thr_3)
        AA = AA3 + ((velocity - Vel_3) / (Vel_4 - Vel_3)) * (AA4 - AA3)
        BB = BB3 + ((velocity - Vel_3) / (Vel_4 - Vel_3)) * (BB4 - BB3)


    elif Vel_4 <= velocity < Vel_5:
        thr = thr_4 + ((velocity - Vel_4) / (Vel_5 - Vel_4)) * (thr_5 - thr_4)
        AA = AA4 + ((velocity - Vel_4) / (Vel_5 - Vel_4)) * (AA5 - AA4)
        BB = BB4 + ((velocity - Vel_4) / (Vel_5 - Vel_4)) * (BB5 - BB4)


    elif Vel_5 <= velocity < Vel_6:
        thr = thr_5 + ((velocity - Vel_5) / (Vel_6 - Vel_5)) * (thr_6 - thr_5)
        AA = AA5 + ((velocity - Vel_5) / (Vel_6 - Vel_5)) * (AA6 - AA5)
        BB = BB5 + ((velocity - Vel_5) / (Vel_6 - Vel_5)) * (BB6 - BB5)

    elif Vel_6 <= velocity < Vel_7:
        thr = thr_6 + ((velocity - Vel_6) / (Vel_7 - Vel_6)) * (thr_7 - thr_6)
        AA = AA6 + ((velocity - Vel_6) / (Vel_7 - Vel_6)) * (AA7 - AA6)
        BB = BB6 + ((velocity - Vel_6) / (Vel_7 - Vel_6)) * (BB7 - BB6)


    elif Vel_7 <= velocity < Vel_8:
        thr = thr_7 + ((velocity - Vel_7) / (Vel_8 - Vel_7)) * (thr_8 - thr_7)
        AA = AA7 + ((velocity - Vel_7) / (Vel_8 - Vel_7)) * (AA8 - AA7)
        BB = BB7 + ((velocity - Vel_7) / (Vel_8 - Vel_7)) * (BB8 - BB7)

    elif Vel_8 <= velocity < Vel_9:
        thr = thr_8 + ((velocity - Vel_8) / (Vel_9 - Vel_8)) * (thr_9 - thr_8)
        AA = AA8 + ((velocity - Vel_8) / (Vel_9 - Vel_8)) * (AA9 - AA8)
        BB = BB8 + ((velocity - Vel_8) / (Vel_9 - Vel_8)) * (BB9 - BB8)

    elif Vel_9 <= velocity < Vel_10:
        thr = thr_9 + ((velocity - Vel_9) / (Vel_10 - Vel_9)) * (thr_10 - thr_9)
        AA = AA9 + ((velocity - Vel_9) / (Vel_10 - Vel_9)) * (AA10 - AA9)
        BB = BB9 + ((velocity - Vel_9) / (Vel_10 - Vel_9)) * (BB10 - BB9)

    else:
        thr = thr_10
        AA = AA10
        BB = BB10





    return thr, AA, BB
