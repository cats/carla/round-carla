'''
* Copyright 2021-2022 EURECOM
* Author - Nadar Ali (ali.nadar@eurecom.fr)
* Date created - 11/10/2021
'''

import glob
import os
import sys
import datetime
import csv
import traceback
from thr import transfer_function_interpolate
#from simple_pid import PID
from operator import attrgetter
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla
import threading
import logging
import time
import numpy as np
import math
import matplotlib.pyplot as plt
import pygame
import argparse
import json
from pid_test_update_ic_fcn_final_merge_speedFollow import adv_thr_ctrl

#region "Variables"
IDs_tl_ordered = [9, 16, 13, 19, 20, 22, 21, 26, 33, 29, 31, 35, 38, 39, 40, 44, 45, 52, 50, 51, 54, 53, 61, 65, 63, 64, 66, 70, 73, 74, 75, 85, 83, 94, 90, 92, 97, 101, 100, 103, 110, 107, 108, 109, 111, 113, 119, 120, 126, 130, 138, 150, 151, 153, 155, 168, 170, 173, 175, 177, 178, 179, 183, 188, 185, 187, 192, 195, 193, 196, 197, 198, 210, 212, 213, 214, 216, 219, 222, 223, 224, 232, 238, 237, 242, 243, 249, 248, 250, 251, 252, 254, 255, 257, 258, 260, 259, 263, 264, 265]
IDs_tr_ordered = [28, 201]
IDs_bl_ordered = [17, 25, 37, 41, 47, 68, 88, 104, 105, 106, 112, 117, 124, 125, 131, 134, 144, 145, 157, 159, 167, 184, 186, 189, 204, 215, 220, 225, 229, 233, 239, 247, 261]
IDs_br_ordered = [10, 11, 12, 14, 18, 23, 24, 27, 32, 30, 36, 34, 43, 42, 46, 48, 49, 55, 56, 57, 58, 59, 60, 62, 67, 69, 71, 72, 76, 77, 78, 80, 81, 82, 84, 86, 87, 89, 91, 93, 95, 96, 98, 99, 102, 268, 114, 115, 116, 118, 121, 122, 123, 127, 128, 129, 132, 133, 136, 137, 135, 140, 139, 141, 142, 143, 146, 147, 148, 149, 152, 154, 158, 156, 160, 161, 162, 163, 165, 164, 166, 169, 171, 172, 174, 176, 180, 181, 182, 190, 191, 194, 199, 200, 202, 203, 205, 206, 207, 208, 209, 211, 217, 218, 221, 226, 230, 227, 231, 228, 234, 235, 236, 240, 241, 244, 245, 246, 253, 256, 262, 267, 266]

list_model_throttles, list_errors = [],[]
IM_WIDTH = 320
IM_HEIGHT = 240
actors =[]
vehicles, vehicles_running = [],[]
object_counter = 1
k_e =  18.9
k_v =  2.2
tm_port = 8000
CENTER = carla.Location(x=0, y=0, z=0.5)
COLOR_CARLA_GREEN = carla.Color(0, 200, 0)
BIG_RADIUS = 14
Distance_wp_before_follow = 40
Distance_binding = 40
waypoints,sides = [],[]
side_tr,side_tl,side_br,side_bl = [],[],[],[]
frame_compact = 0
total_actors_count = 0
Distance_auto_to_stop = 1
spawn_points = []
sp_tr_1, sp_tl_1, sp_br_1, sp_bl_1 = None,None, None,None
sp_tr_2, sp_tl_2, sp_br_2, sp_bl_2 = None,None, None,None
sp_tr_3, sp_tl_3, sp_br_3, sp_bl_3 = None,None, None,None
next_toSpawn  = None
world,debug, ego_bp, blueprints = None, None, None, None
actors = []
frame = 0
indexer_sp = 0
loop = True
index_road_br,index_road_bl,index_road_tr,index_road_tl  = 0,0,0,0
#endregion

#region "Functions"
def angle_directions(ego_transform,heading):
    ego_location = ego_transform.location
    forward_vector_ego = ego_transform.get_forward_vector()
    direction_ego = ego_transform.location + (2 * forward_vector_ego)
    angle_wp = (heading + 360) % 360
    angle_ego = (math.degrees(math.atan2(direction_ego.y - ego_location.y, direction_ego.x - ego_location.x))+ 360) % 360
    angle = angle_wp - angle_ego
    if angle > 90: angle = (angle_wp - 360) - angle_ego
    if angle < -90: angle = (angle_wp + 360) - angle_ego
    return angle

def getDistance(location1,location2):
    return round(math.sqrt(((location1.x - location2.x) ** 2) + ((location1.y - location2.y) ** 2)), 2)

def getSpeed(actor):
    speed_x, speed_y = actor.get_velocity().x, actor.get_velocity().y
    speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
    return speed

def lineMagnitude (x1, y1, x2, y2):
    lineMagnitude = math.sqrt(math.pow((x2 - x1), 2) + math.pow((y2 - y1), 2))
    return lineMagnitude
#Calc minimum distance from a point and a line segment (i.e. consecutive vertices in a polyline).
def DistancePointLine(px, py, x1, y1, x2, y2):
    #http://local.wasp.uwa.edu.au/~pbourke/geometry/pointline/source.vba
    LineMag = lineMagnitude(x1, y1, x2, y2)

    if LineMag < 0.00000001:
        DistancePointLine = 9999
        return DistancePointLine

    u1 = (((px - x1) * (x2 - x1)) + ((py - y1) * (y2 - y1)))
    u = u1 / (LineMag * LineMag)

    if (u < 0.00001) or (u > 1):
        #// closest point does not fall within the line segment, take the shorter distance
        #// to an endpoint
        ix = lineMagnitude(px, py, x1, y1)
        iy = lineMagnitude(px, py, x2, y2)
        if ix > iy:
            DistancePointLine = iy
        else:
            DistancePointLine = ix
    else:
        # Intersecting point is on the line, use the formula
        ix = x1 + u * (x2 - x1)
        iy = y1 + u * (y2 - y1)
        DistancePointLine = lineMagnitude(px, py, ix, iy)

    return DistancePointLine

def get_nearest_wp(tranform, wps):
    old_d= 1000
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(tranform.location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            item = wps[i]
            index = i
    return index, item

def get_nearest_wp_by_location(location, wps):
    old_d= 1000
    wp=None
    index = None
    for i in range(len(wps)):
        d = getDistance(location, wps[i].transform.location)
        if d < old_d:
            old_d = d
            item = wps[i]
            index = i
    return index, item

def get_nearest_index(t, wps, index_back,bumper):
    global flag
    direction = t.get_forward_vector()
    min_d= float("inf")
    index = 0
    my_x,my_y = bumper.x, bumper.y

    for i in range(index_back-5,index_back+6):
        if i < 0:continue
        elif i > len(wps)-2 : continue
        else:
            wp1, wp2 = wps[i], wps[i + 1]
            wp1_x, wp1_y = wp1.transform.location.x, wp1.transform.location.y
            wp2_x, wp2_y = wp2.transform.location.x, wp2.transform.location.y
            d = DistancePointLine(my_x, my_y, wp1_x, wp1_y, wp2_x, wp2_y)
            if d < min_d:
                min_d = d
                index = i
    return index

def getSide(x,y):
    if x > 0 and y > 0: return "tr"
    if x > 0 and y < 0: return "tl"
    if x < 0 and y > 0: return "br"
    if x < 0 and y < 0: return "bl"

def removeFromSide(vehicle):
    global sides, side_tr,side_tl,side_br,side_bl
    if vehicle.side =="tr":
        if vehicle == side_tr.first:
            side_tr.first = None
        side_tr.vehicles.remove(vehicle)

    if vehicle.side =="tl":
        if vehicle == side_tl.first:
            side_tl.first = None
        side_tl.vehicles.remove(vehicle)

    if vehicle.side =="br":
        if vehicle == side_br.first:
            side_br.first = None
        side_br.vehicles.remove(vehicle)

    if vehicle.side =="bl":
        if vehicle == side_bl.first:
            side_bl.first = None
        side_bl.vehicles.remove(vehicle)

    sides = [side_tl, side_tr, side_bl,  side_br]

def sign(x):
    if x >= 0: return 1
    else: return -1

def plot(l1,l2):
    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 16,
            }
    plt.title(f'Carla vs RounD', fontdict=font)
    plt.xlabel('Frames - 25 fps', fontdict=font)
    plt.ylabel('Speed (km/h)', fontdict=font)
    plt.plot(l1, l2, label="current_speed")  # plot first line
    plt.legend(loc="upper right")
    plt.show()


def getSideSpawnPoint(side):
    global  index_road_br,index_road_bl,index_road_tr,index_road_tl
    sp = None
    if side == "br":
        if index_road_br == 0:
            sp = sp_br_1
        elif index_road_br == 1:
            sp = sp_br_2
        else:
            sp = sp_br_3
        index_road_br = (index_road_br +1) % 3
    if side == "bl":
        if index_road_bl == 0:
            sp = sp_bl_1
        elif index_road_bl==1:
            sp = sp_bl_2
        else:
            sp = sp_bl_3
        index_road_bl = (index_road_bl +1) % 3
    if side == "tr":
        if index_road_tr == 0:
            sp = sp_tr_1
        elif index_road_tr == 1:
            sp = sp_tr_2
        else:
            sp = sp_tr_3
        index_road_tr = (index_road_tr +1) % 3

    if side == "tl":
        if index_road_tl == 0:
            sp = sp_tl_1
        elif index_road_tl == 1:
            sp = sp_tl_2
        else:
            sp = sp_tl_3
        index_road_tl = (index_road_tl + 1) % 3

    return sp
#endregion

class Record(object):
    x:float
    y:float
    heading:float
    speed:float
    longAcc:float
    frame:int
    transform: carla.Transform
    speed_km : float
    toCenter: float
    def __init__(self,x,y,heading, speed,longAcc, frame):
        self.x = x
        self.y = y
        self.heading = heading
        self.speed = speed
        self.longAcc = longAcc
        self.frame = frame
        location = carla.Location(self.x, self.y, 0)
        rotation = carla.Rotation(pitch=0, yaw=self.heading, roll=0)
        self.transform = carla.Transform(location, rotation)
        self.speed_km = self.speed * 3.6
        self.toCenter = getDistance(location, CENTER)
    def updatePoint(self,_x,_y):
        self.x = _x
        self.y = _y
        location = carla.Location(self.x, self.y, 0)
        self.transform = carla.Transform(location, self.transform.rotation)


class Vehicle(object):
    # region "Vehicle variables"
    actor:carla.Vehicle = None
    trackId: int
    pre_initialFrame: int
    initialFrame: int
    finalFrame: int
    numFrames: int
    #totalAdditionalFrames: int
    width:float
    length: float
    vehClass: str
    records = []
    list_distance_center = []
    list_current_speed = []
    list_target_speed = []
    list_target_longAcc = []
    list_frame_follow = []
    list_throttle = []
    list_steer = []
    list_brake = []
    throttle_list = []
    throttle_index_current:int = 0
    start_transform: carla.Transform
    start_move_centroid: carla.Location
    start_follow_centroid: carla.Location
    start_distance_to_center: int = 100
    distance_to_center: int
    distance_to_follow: int
    distance_move_center:int
    distance_follow_center:int
    transform: carla.Transform
    centroid: carla.Location
    steer: float = 0
    max_steering_angle: float = 0
    max_steering_angle_rad: float = 0
    throttle: float = 0
    brake: float = 0
    brake_index :int
    list_follow_brake : []
    list_follow_throttle : []
    centroid: carla.Location
    bumper: carla.Location
    direction: carla.Location
    heading: float
    yaw: float
    speed: float = 0
    speed_km: float = 0
    index_back: int = 0
    index_front: int = 0
    current_target_velocity: int = 20
    current_target_longAcc: int = 0
    side:str
    status:str
    changing:bool
    direction : str
    distance_wp_before_follow : int
    wp_initial: carla.Waypoint
    wp_wait: carla.Waypoint
    ##### Initial Throttle #######
    initial_target_speed = 0
    initial_throttle = 0
    additional_frames: int
    additional_distance: float
    applied_first_throttle:bool
    spawned:bool = False
    start_fast: bool = False
    list_error_frames = []
    error: int = 0
    #endregion
    def __init__(self,trackId,initialFrame,finalFrame,numFrames,width,length,vehClass):
        self.applied_first_throttle = False
        self.actor = None
        self.steer = 0
        self.throttle = 0
        self.brake = 0
        self.brake_index = 0
        self.list_follow_brake =    [0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.list_follow_throttle = [0.27,0.4,0.46,0.48,0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58]
        self.trackId = trackId
        self.initialFrame = initialFrame
        self.pre_initialFrame = 0
        self.finalFrame = finalFrame
        self.numFrames = numFrames
        self.additionalFrames = 0
        self.totalAdditionalFrames = 0
        self.width = width
        self.length = length
        self.vehClass = vehClass
        self.transform = None
        self.centroid = None
        self.bumper = None
        self.max_steering_angle = 70
        self.side = ""
        self.status = "autopilot"
        self.changing = False
        self.records = []
        self.list_distance_center = []
        self.list_current_speed = []
        self.list_target_speed = []
        self.list_target_longAcc = []
        self.list_frame_follow = []
        self.list_throttle = []
        self.list_steer = []
        self.list_brake = []
        self.distance_to_center = 100
        self.direction = "entering"
        self.distance_wp_before_follow = 0
        self.wp_initial = None
        self.wp_wait = None
        self.list_error_frames = []
        self.error = 0

    def assignThrottleModel(self):
        for i in range(len(list_model_throttles)):
            if list_model_throttles[i][1] > self.records[0].speed_km:
                t = list_model_throttles[i-1][0] + ((self.records[0].speed_km - list_model_throttles[i-1][1])/( list_model_throttles[i][1] -  list_model_throttles[i-1][1])) * (list_model_throttles[i][0] - list_model_throttles[i-1][0])
                #print(f"My model Throttle is :{list_model_throttles[i][0]}")
                #print(f"My traget speed is :{self.records[0].speed_km}")
                self.initial_throttle = t
                #self.initial_throttle =  list_model_throttles[i][0]
                #self.initial_target_speed = list_model_throttles[i][1]
                self.additional_frames = list_model_throttles[i][2]
                self.additional_distance = list_model_throttles[i][3]
                break

        self.pre_initialFrame = self.initialFrame - self.additional_frames
        # print(f"Veh-{self.trackId}---> throttle:{self.initial_throttle} "
        #                          f"target_speed:{self.initial_target_speed} "
        #                              f"distance:{self.additional_distance} "
        #                          f"added_frames:{self.additional_frames}")

    def updateInitialFrameAndWaypoints(self):
        global indexer_sp
        list_additional,list_between,list_join = [],[],[]
        if self.side == "tl":
            sp = sp_tl[indexer_sp]
            indexer_sp = (indexer_sp + 1) % 3
        elif self.side == "tr":
            sp = sp_tr[indexer_sp]
            indexer_sp = (indexer_sp + 1) % 3
        elif self.side == "bl":
            sp = sp_bl[indexer_sp]
            indexer_sp = (indexer_sp + 1) % 3
        elif self.side == "br":
            sp = sp_br[indexer_sp]
            indexer_sp = (indexer_sp + 1) % 3
        _, self.wp_initial = get_nearest_wp(sp, waypoints)
        wp = self.wp_initial
        heading = self.records[0].heading
        speed = self.records[0].speed
        longAcc = self.records[0].longAcc
        while getDistance(wp.transform.location, self.start_follow_centroid) > self.additional_distance:
            wp = wp.next(0.05)[0]
        self.wp_initial = wp

       # find middle [point for smoothing steer
        jump = 25
        x_0, y_0 = self.records[0].x, self.records[0].y
        x_jump, y_jump = self.records[jump].x, self.records[jump].y
        delta_x, delta_y = x_0 - x_jump, y_0 - y_jump
        middle_x, middle_y = x_0 + delta_x, y_0 + delta_y

        x1, y1 = wp.transform.location.x, wp.transform.location.y
        x3, y3 = middle_x, middle_y

        _, middle_wp = get_nearest_wp_by_location(carla.Location(middle_x,middle_y), waypoints)
        wp_temp = wp

        while getDistance(wp_temp.transform.location,middle_wp.transform.location) > 1:
            wp_temp = wp_temp.next(0.5)[0]
            list_additional.append(Record(wp_temp.transform.location.x, wp_temp.transform.location.y, wp_temp.transform.rotation.yaw, speed, longAcc, -1))


        x4, y4 = self.start_follow_centroid.x, self.start_follow_centroid.y

        self.start_move_centroid = carla.Location(x1,y1)
        self.transform = wp.transform
        self.distance_move_center = getDistance(self.start_move_centroid, CENTER)


        step_x, step_y = (x4 - x3) / jump, (y4 - y3) / jump
        for i in range(jump):
            x, y = x3 + (i * step_x), y3 + (i * step_y)
            list_between.append(Record(x, y, heading, speed,longAcc, -1))

        x2,y2 = list_additional[-jump].x, list_additional[-jump].y
        step_x, step_y = (x4 - x2) / (2 * jump), (y4 - y2) / (2 * jump)
        for i in range(2 * jump):
            x, y = x2 + (i * step_x), y2 + (i * step_y)
            list_join.append(Record(x, y, heading, speed,longAcc, -1))

        #print(f"{self.trackId}: additional Distance: {getDistance(location1,location2)}")
        self.distance_wp_before_follow = getDistance(list_additional[0], self.start_follow_centroid)

        list_additional = list_additional[:-jump]

        self.records.pop(0) # to remove the first index for better steering
        time_of_line = int((self.finalFrame - self.pre_initialFrame)/25)

        self.records = list_additional + list_join + self.records
        self.wp_wait = wp

    def setRecord(self, frame,x,y,heading,speed_x,speed_y, long_acc):
        frame = int(frame)
        x = float(x) - 137  # -144
        y = -(float(y) + 60)  # +57
        #debug.draw_string(carla.Location(x, y, 0), ".", False, carla.Color(255, 0, 0), 3000)
        heading = 360 - float(heading)
        if heading > 180: heading = heading - 360
        speed_x, speed_y = float(speed_x), float(speed_y)
        speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
        longAcc = float(long_acc)
        record = Record(x, y, heading, speed,longAcc,frame)
        if len(self.records) == 1:  # initial location of Vehicle
            location = carla.Location(x, y, 0)
            rotation = carla.Rotation(pitch=0, yaw=heading, roll=0)
            self.initial_target_speed = record.speed_km
            self.start_follow_centroid = location
            self.distance_follow_center = getDistance(self.start_follow_centroid, CENTER)
            self.start_transform = carla.Transform(location, rotation)
            self.start_distance_to_center = getDistance(self.start_follow_centroid, CENTER)
            self.side = getSide(x,y)
        self.records.append(record)

    def updatePoints(self):
        i = 0
        while i < len(self.records) - 1:
            first_pos = carla.Location(self.records[i].x, self.records[i].y, 0)
            second_pos = carla.Location(self.records[i + 1].x, self.records[i + 1].y, 0)
            distance_between = getDistance(first_pos, second_pos)
            if distance_between < 0.03:
                j = i + 1
                while distance_between < 0.03:
                    # world.debug.draw_string(second_pos, f"{round(distance_between, 2)}", False, carla.Color(255, 0, 0), 500)
                    pos1 = carla.Location(self.records[j].x, self.records[j].y, 0)
                    pos2 = carla.Location(self.records[j + 1].x, self.records[j + 1].y, 0)
                    distance_between = getDistance(pos1, pos2)
                    j += 1
                iterations = j - (i - 1)
                fixedPoints = getFixedListPoints(self.records[i - 1], self.records[j], iterations)
                counter = 0
                for k in range(i - 1, j + 1):
                    self.records[k].updatePoint(fixedPoints[counter][0],fixedPoints[counter][1])
                    counter += 1
                i = j
            else:
                i += 1

    def update(self,frame,debug):
        global vehicles, vehicles_running, loop, index_vehicles
        try:
            if not self.actor or not self.actor.is_alive: return
            # region "Updating Vehicle"
            self.transform = self.actor.get_transform()
            fwd_vector = self.transform.get_forward_vector()
            self.max_steering_angle_rad = math.radians(self.max_steering_angle)
            self.steer = 0  # self.actor.get_control().steer
            self.centroid = self.transform.location + carla.Location(z=1)
            self.bumper = self.centroid + (self.actor.bounding_box.extent.x * fwd_vector)
            self.distance_to_center = getDistance(self.centroid, CENTER)
            self.distance_to_follow = getDistance(self.centroid, self.start_follow_centroid)
            _yaw = self.transform.rotation.yaw
            self.yaw = _yaw if _yaw > 0 else _yaw + 360
            speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
            self.speed = math.sqrt(speed_x ** 2 + speed_y ** 2)
            self.speed_km = round(3.6 * self.speed, 2)
            # endregion
            if  self.distance_to_center > self.distance_move_center: #and self.status != "follow":
                self.status = "autopilot"
                pass # Autopilot
            else:
                if frame < self.pre_initialFrame:
                    if self.status != "wait":
                        self.status = "wait"
                        self.changing = True

                if self.pre_initialFrame <= frame < self.initialFrame:
                    if self.status != "move":
                        self.status = "move"
                        self.changing = True

                if frame >= self.initialFrame:
                    if self.status !="follow":
                        #print("ID: " + str(self.trackId) + " -> Error Distance: "+ str(self.distance_to_center-self.distance_follow_center))
                        self.status = "follow"
                        self.changing = True

                    #region "Deleting"
                    if len(self.records)-1 == self.index_front:
                        self.actor.destroy()
                        self.actor = None
                        vehicles.remove(self)
                        addVehicle(self.side)
                        del self
                        return
                    #endregion

            debug.draw_string(self.centroid, f"{self.trackId}", False,carla.Color(255, 255, 0) if frame >= self.initialFrame else carla.Color(0, 255, 0), 0.05)
            self.setControl(self.status,frame,debug) # Applying control based on status
        except Exception:
            traceback.print_exc()
            pass

    def calculateError(self):
        error_diff = self.initialFrame - int(sum(self.list_error_frames) / len(self.list_error_frames))
        print(error_diff)

    def correctExceptionally(self):
        early = 0
        if self.trackId == 53: early = 35
        if self.trackId == 54: early = 35
        if self.trackId == 110: early = 135
        self.pre_initialFrame += early
        self.initialFrame += early

    def plot_measurement(self):
        font = {'family': 'serif',
                'color': 'darkred',
                'weight': 'normal',
                'size': 16,
                }
        plt.title(f'Carla vs RounD', fontdict=font)
        plt.xlabel('Frames - 25 fps', fontdict=font)
        plt.ylabel('Speed (km/h)', fontdict=font)
        plt.plot(self.list_frame_follow, self.list_current_speed, label="current_speed")  # plot first line
        plt.plot(self.list_frame_follow, self.list_target_speed, label="target_speed")  # plot second line
        plt.plot(self.list_frame_follow, self.list_target_longAcc, label="target_longAcc")  # plot second line
        plt.axvline(x=self.pre_initialFrame, color='gray')
        plt.axvline(x=self.initialFrame, color='gray')
        # additional plots
        #plt.plot(self.list_frame_follow, self.list_steer, label="steer")  # plot second line
        plt.plot(self.list_frame_follow, self.list_brake, label="brake")  # plot second line
        plt.plot(self.list_frame_follow, self.list_throttle, label="throttle")  # plot second line
        #plt.plot(self.list_frame_follow, self.list_distance_center, label="dist_center")  # plot second line
        plt.legend(loc="upper right")
        #plt.show()
        plt.savefig(f'data/plot_round_{self.trackId}.png')
        plt.close()

    def calculateSteer(self,frame,debug):
        center_axle_x, center_axle_y = self.bumper.x, self.bumper.y
        self.index_back = get_nearest_index(self.transform, self.records, self.index_back, self.bumper)

        self.index_front = self.index_back + 1
        back = self.records[self.index_back]
        front = self.records[self.index_front]
        back_x , back_y  = back.transform.location.x ,  back.transform.location.y
        front_x, front_y = front.transform.location.x, front.transform.location.y
        yaw_path = np.arctan2(front_y - back_y, front_x - back_x)
        yaw_diff = yaw_path - math.radians(self.yaw)
        if yaw_diff > np.pi:   yaw_diff -= 2 * np.pi
        if yaw_diff < - np.pi: yaw_diff += 2 * np.pi
        speed_x, speed_y = self.actor.get_velocity().x, self.actor.get_velocity().y
        v = math.sqrt(speed_x ** 2 + speed_y ** 2)
        crosstrack_error = DistancePointLine(center_axle_x, center_axle_y, back_x, back_y, front_x, front_y)
        yaw_cross_track = np.arctan2(center_axle_y - front_y, center_axle_x - front_x)
        yaw_path2ct = yaw_path - yaw_cross_track
        if yaw_path2ct > np.pi:   yaw_path2ct -= 2 * np.pi
        if yaw_path2ct < - np.pi: yaw_path2ct += 2 * np.pi
        if yaw_path2ct > 0:
            crosstrack_error = abs(crosstrack_error)
        else:
            crosstrack_error = - abs(crosstrack_error)
        yaw_diff_crosstrack = np.arctan(k_e * crosstrack_error / (k_v + v))
        steer_expect = yaw_diff + yaw_diff_crosstrack
        if steer_expect > np.pi:   steer_expect -= 2 * np.pi
        if steer_expect < - np.pi: steer_expect += 2 * np.pi
        steer_expect = min(self.max_steering_angle_rad, steer_expect)
        steer_expect = max(-self.max_steering_angle_rad, steer_expect)
        steer_normalized = steer_expect / self.max_steering_angle_rad
        dc = round(self.distance_to_center,2)
        diff_dist = round(back.toCenter - front.toCenter)

        if 2.5*BIG_RADIUS > self.distance_to_center >= BIG_RADIUS and abs(steer_normalized) > 0.5:
            steer_normalized = sign(steer_normalized) * 0.1

        self.steer = steer_normalized
        #debug.draw_string(self.centroid, "*", False, carla.Color(0, 255, 0), 150)
        #debug.draw_string(self.centroid, f"{round(steer_normalized, 1)}", False, carla.Color(0, 255, 0), 150)

    def calculateThrottle(self, frame):
        diff = (self.speed_km - self.current_target_velocity)
        coeff = 1
        if frame < self.initialFrame + 1:
            pass
        else:
            try:
                self.brake = 0
                if self.throttle_index_current == 0:
                    records_at_25frames = [x for x in self.records if x.frame == frame + 4]
                    if len(records_at_25frames) > 0:
                        target_speed_at_25frames_km = records_at_25frames[0].speed_km
                        coeff = 1.06
                        self.throttle_list = adv_thr_ctrl(self.throttle, self.speed_km / 3.6, target_speed_at_25frames_km * coeff)
                        # print(f"current_trottle:{self.throttle} Speed_km: {self.speed_km}  target_speed: {target_speed_at_25frames_km}")
                        # print(self.throttle_list)
                    else:
                        return

                self.throttle = self.throttle_list[self.throttle_index_current]
                self.throttle_index_current += 1
                if self.throttle_index_current == len(self.throttle_list): self.throttle_index_current = 0
            except Exception:
                traceback.print_exc()

        if diff > 0.5: self.throttle = self.throttle * 0.9

        self.throttle = min(1, self.throttle)

        if self.throttle == 0 and diff > 0.2 and self.current_target_velocity < self.initial_target_speed / 2:
            if self.speed_km > 5:
                self.brake = diff * (self.speed_km / self.initial_target_speed)
            else:
                self.brake = (diff * (self.speed_km / self.initial_target_speed)) / 10

        if self.current_target_velocity < 0.25:
            self.brake = 1

    def setControl(self,status, frame,debug):
        if status == "autopilot":
            self.actor.set_autopilot(True)
        else:
            self.actor.set_autopilot(False)
            control = self.actor.get_control()
            control.manual_gear_shift = False
            if self.status == "wait":
                self.brake = 1
                self.steer = 0
                self.throttle = 0
            elif self.status == "move":
                self.brake = 0
                self.calculateSteer(frame,debug) #self.steer = 0
                if not self.applied_first_throttle:
                    control.manual_gear_shift = True
                    control.gear = 1
                    self.applied_first_throttle = True
                self.throttle = self.initial_throttle


            elif self.status == "follow":
                self.calculateSteer(frame,debug)
                self.calculateThrottle(frame)


            control.steer = self.steer
            control.throttle = self.throttle
            control.brake = self.brake
            self.actor.apply_control(control)

            if self.status in ["move","follow"]:
                # collect data fro plotting...........................................

                try:
                    rec = [x for x in self.records if x.frame == frame][0]
                    self.current_target_velocity = rec.speed_km
                    self.current_target_longAcc = rec.longAcc
                    #self.list_distance_center.append(self.distance_to_center)
                    self.list_throttle.append(self.throttle * 100)
                    self.list_steer.append(self.steer * 100)
                    self.list_brake.append(self.brake * 100)
                    self.list_target_speed.append(self.current_target_velocity)
                    self.list_target_longAcc.append(self.current_target_longAcc* 10)
                    self.list_current_speed.append(self.speed_km)
                    self.list_frame_follow.append(frame)
                except Exception:
                    pass

def getFixedListPoints(record_start, record_end, iterations):
    p_start = (record_start.x, record_start.y)
    p_end = (record_end.x, record_end.y)
    list = np.linspace(p_start, p_end, iterations + 1)
    return list

def spawnOne(veh):
    global world, ego_bp, actors
    try:
        sp = getSideSpawnPoint(veh.side)
        actor = world.spawn_actor(ego_bp, sp)
        actor.set_transform(veh.wp_initial.transform)
        actor.set_autopilot(False)
        actors.append(actor)
        veh.spawned = True
        veh.actor = actor
    except Exception:
        traceback.print_exc()
        pass

def getIDs(side):
    if side =="bl": return IDs_bl_ordered[:10]
    if side =="br": return IDs_br_ordered[:10]
    if side =="tl": return IDs_tl_ordered[:10]
    if side =="tr": return IDs_tr_ordered[:10]

def initiateSpawnPoints(map):
    global  spawn_points
    spawn_points = map.get_spawn_points()
    global sp_tr, sp_br, sp_tl, sp_bl
    global sp_tr_1, sp_br_1, sp_tl_1, sp_bl_1
    global sp_tr_2, sp_br_2, sp_tl_2, sp_bl_2
    global sp_tr_3, sp_br_3, sp_tl_3, sp_bl_3
    for spawn_point in spawn_points:
        x, y = round(spawn_point.location.x), round(spawn_point.location.y)
        # world.debug.draw_string(carla.Location(x,y,0), f"{x , y}", False, carla.Color(255, 0, 0), 500)

        if x == -602 and y == 495:sp_br_1 = spawn_point
        if x == 489 and y == -738:sp_tl_1 = spawn_point
        if x == 289 and y == 260:sp_tr_1 = spawn_point
        if x == -169 and y == -500:sp_bl_1 = spawn_point

        if x == -523 and y == 440: sp_br_2 = spawn_point
        if x == 151 and y == -232: sp_tl_2 = spawn_point
        if x == 289 and y == 260:  sp_tr_2 = spawn_point
        if x == -107 and y == -297: sp_bl_2 = spawn_point

        if x == -360 and y == 317: sp_br_3 = spawn_point
        if x == 146 and y == -234: sp_tl_3 = spawn_point
        if x == 289 and y == 260:  sp_tr_3 = spawn_point
        if x == -112 and y == -296: sp_bl_3 = spawn_point

    sp_br = [sp_br_1, sp_br_2, sp_br_3]
    sp_bl = [sp_bl_1, sp_bl_2, sp_bl_3]
    sp_tr = [sp_tr_1, sp_tr_2, sp_tr_3]
    sp_tl = [sp_tl_1, sp_tl_2, sp_tl_3]

index_vehicles = 9
list_IDs = []
def addVehicle(side):
    global vehicles, index_vehicles
    index_vehicles += 1
    if side == "bl": list = IDs_bl_ordered
    elif side == "br": list = IDs_br_ordered
    elif side == "tl": list = IDs_tl_ordered
    else: list = IDs_tr_ordered

    if index_vehicles < len(list):
        id = list[index_vehicles]
        vehicle = loadVehicle(id)
        vehicle.updatePoints()
        vehicle.assignThrottleModel()
        vehicle.error = [x[1] for x in list_errors if x[0] == vehicle.trackId][0]
        vehicle.pre_initialFrame = vehicle.pre_initialFrame + vehicle.error
        vehicle.updateInitialFrameAndWaypoints()
        vehicle.correctExceptionally()
        vehicles.append(vehicle)

def strToDatetime(strDateTime):
    _split = strDateTime.split(' ')
    split1 = _split[0].split('-')
    Y, M, D = int(split1[0]), int(split1[1]), int(split1[2])
    split2 = _split[1].split('.')
    MS = int(split2[1])
    split3 = split2[0].split(':')
    H, MN, S = int(split3[0]), int(split3[1]), int(split3[2])
    date_time_obj = datetime.datetime(Y, M, D, H, MN, S, MS)
    return date_time_obj

def loadVehicle(id):
    file_vehicle = open(f"../data/vehicles/vehicle_{id}.json", "r")
    json_vehicle = json.loads(file_vehicle.read())
    trackId = int(json_vehicle["trackId"])
    initialFrame = int(json_vehicle["initialFrame"])
    finalFrame = int(json_vehicle["finalFrame"])
    numFrames = int(json_vehicle["numFrames"])
    width = float(json_vehicle["width"])
    length = float(json_vehicle["length"])
    vehClass = json_vehicle["vehClass"]
    vehicle = Vehicle(trackId, initialFrame, finalFrame, numFrames, width, length, vehClass)
    file_vehicle.close()

    file_frames = open(f"../data/frames/frames_{id}.json", "r")
    json_frames = json.loads(file_frames.read())
    for frameData in json_frames:
        vehicle.setRecord(frameData["frame"],
                          frameData["x"], frameData["y"],
                          frameData["heading"],
                          frameData["speed_x"], frameData["speed_y"],
                          frameData["long_acc"])
    file_frames.close()
    return vehicle

def main():
    #region "Carla Variables"
    argparser = argparse.ArgumentParser(description='CARLA Manual Control Client')
    argparser.add_argument('--side',default='bl', help='Side of RA leg')
    argparser.add_argument('--time',default=None, type=str, help='time to start synchronized simulation')
    args = argparser.parse_args()
    global actors, vehicles, tm_port, waypoints,spawn_points, list_model_throttles
    global frame, blueprints
    global side_tr,side_tl,side_br,side_bl
    global world, ego_bp, debug, loop
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    client = carla.Client('127.0.0.1',2000)
    client.set_timeout(1.0)
    world = client.get_world()
    #endregion
    try:
        #region "Carla Setup"
        debug = world.debug
        carla_map =  world.get_map()
        blueprints = world.get_blueprint_library()
        traffic_manager = client.get_trafficmanager(8000)
        traffic_manager.set_synchronous_mode(True)
        ego_bp = blueprints.find('vehicle.tesla.model3')
        ego_bp.set_attribute('color','255,0,0')
        waypoints = carla_map.generate_waypoints(0.5)
        initiateSpawnPoints(carla_map)
        #endregion
        list_IDs = getIDs(args.side)
        with open(f'../data/models_throttles_new_updated.csv', mode='r') as throttles:
            reader = csv.reader(throttles)
            next(reader, None)
            #throttle, speed, frame, distance
            for row in reader:
                _throttle = float(row[0])
                _speed = float(row[1])
                _frame = int(row[2])
                _distance = float(row[3])
                _data = [_throttle, _speed, _frame, _distance]
                list_model_throttles.append(_data)
            list_model_throttles = sorted(list_model_throttles, key=lambda x: x[1])
        with open(f'errorFrames.csv', mode='r') as errors:
            reader = csv.reader(errors)
            next(reader, None)
            for row in reader:
                _id = int(row[0])
                _error = int(row[1])
                _data = [_id, _error]
                list_errors.append(_data)

        for id in list_IDs:
            vehicle = loadVehicle(id)
            vehicles.append(vehicle)

        for veh in vehicles:
            veh.updatePoints()
            veh.assignThrottleModel()
            vehicle.error = [x[1] for x in list_errors if x[0] == veh.trackId][0]
            veh.pre_initialFrame = veh.pre_initialFrame + veh.error
            veh.correctExceptionally()

        vehicles = sorted(vehicles, key=lambda x: x.pre_initialFrame)

        for veh in vehicles:
            veh.updateInitialFrameAndWaypoints()

        date_time_obj = strToDatetime(args.time)
        now = datetime.datetime.now()
        while date_time_obj > now: now = datetime.datetime.now()

        frame = -1000
        print(f"Start of Loop ----------> {datetime.datetime.now()}")
        second, counter = 0,0
        clock = pygame.time.Clock()
        while True:
            for veh in vehicles:
                if veh.spawned:
                    veh.update(frame,debug)

                elif frame == veh.pre_initialFrame:
                    print(f"Spawn---> {veh.trackId}")
                    spawnOne(veh)

            frame += 1
            #region "FPS"
            real_second = datetime.datetime.now().second
            if real_second == second:
                counter += 1
            else:
                print(f"FPS: {counter}", end = '')
                print('\r', end='')
                second = real_second
                counter = 1
            #endregion
            clock.tick_busy_loop(25)

    except Exception:
        traceback.print_exc()
        pass
    finally:
        print("All destroyed")

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        print('\nDone with ego vehicle.')
