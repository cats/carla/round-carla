'''
* Copyright 2021-2022 EURECOM
* Author - Nadar Ali (ali.nadar@eurecom.fr)
* Date created - 11/10/2021
'''

import glob
import os
import sys
import time
import traceback
import sys
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
import carla
import logging
import pygame
from datetime import datetime, timedelta
import json


#region "Functions"
def get_world(client, path):
    with open(path) as od_file:
        try:
            data = od_file.read()
        except OSError:
            print('file could not be readed.')
            sys.exit()
    print('load opendrive map %r.' % os.path.basename(path))
    vertex_distance = 2.0  # in meters
    max_road_length = 500.0  # in meters
    wall_height = 0.0  # in meters
    extra_width = 0.6  # in meters
    world = client.generate_opendrive_world(data, carla.OpendriveGenerationParameters(vertex_distance=vertex_distance,
                                                                                      max_road_length=max_road_length,
                                                                                      wall_height=wall_height,
                                                                                      additional_width=extra_width,
                                                                                      smooth_junctions=True,
                                                                                      enable_mesh_visibility=True))
    return world


logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
client = carla.Client('127.0.0.1',2000)
client.set_timeout(5.0)
world = get_world(client, "../RoadRunnerMap/magic-5.xodr")


try:
    #region "Carla Setup"
    world.set_weather(carla.WeatherParameters. ClearSunset)
    spectator = world.get_spectator()
    spectator.set_transform(carla.Transform(carla.Location(x=0, y=0, z=60), carla.Rotation(pitch=-90, roll=-90)))
    #endregion

    _time = datetime.now() + timedelta(seconds=30)
    all_processes = [f'gnome-terminal -t Process_{x} --command "python3.7 vehicles_side_controller.py --side {x}  --time \'{str(_time)}\' "' for x in ['tl','tr','bl','br']]

    for x in all_processes:
        try:
            os.system(x)
            time.sleep(5)
        except Exception:
            pass

    second, counter = 0,0
    frame =1
    clock = pygame.time.Clock()
    while True:
        try:
            frame += 1
            # region "FPS"
            real_second = datetime.datetime.now().second
            if real_second == second:
                counter += 1
            else:
                print(f"FPS: {counter}", end='')
                print('\r', end='')
                second = real_second
                counter = 1
            # endregion
            clock.tick_busy_loop(25)
        except Exception:
            pass
except Exception:
    traceback.print_exc()
    pass
finally:
    print("All Done")

