'''
* Copyright 2021-2022 EURECOM
* Author - Mouradi Sina (mouradi@eurecom.fr)
* Date created - 10/06/2022
'''

import control.matlab as ctrl_matlab
import numpy
import time


def adv_thr_ctrl(thr_0, v_0, v_f, pred_horizon=0.04, ctrl_horizon=0.04, frame_ahead = 25, dt=0.04):  # v_0 m/s   &   v_f km/h

    # This is the longitudinal control function based on system identification for Tesla Model3 in CARLA.
    # The controller takes into account the passenger comfort (Acc_max = 2 m/s^2 , Jerk_max = 0.9 m/s^3).
    # The study for the passenger comfort was done in MATLAB.

    global SIM_TIME, saturated_throttle, time_step, target_speed, ctrlInput, outPID, estDistance, estVelocity

    SIM_TIME = pred_horizon  # sec
    time_step = dt  # sec

    target_speed = v_f / 3.6  # km/h to m/s
    saturated_throttle = 1

    initial_acc = 0
    initial_vel = v_0  # m/s
    initial_thr = thr_0

    ctrlInput = []
    outPID = []
    estDistance = []
    estVelocity = []

    global k_p, k_i, k_d
    k_p, k_i, k_d = 1, 0, 0  # Gains of the PID Controller (studied in MATLAB)

    global Vel_1, Vel_2, Vel_3, Vel_4, Vel_5, Vel_6, Vel_7

    Vel_1 = 3.32  # The s.s. velocity(m/s) that corrresponds to 30% throttle
    Vel_2 = 4.24  # The s.s. velocity(m/s) that corrresponds to 35% throttle
    Vel_3 = 5.36  # The s.s. velocity(m/s) that corrresponds to 40% throttle
    Vel_4 = 6.74  # The s.s. velocity(m/s) that corrresponds to 45% throttle
    Vel_5 = 8.12  # The s.s. velocity(m/s) that corrresponds to 50% throttle
    Vel_6 = 9.52  # The s.s. velocity(m/s) that corrresponds to 55% throttle
    Vel_7 = 11.1  # The s.s. velocity(m/s) that corrresponds to 60% throttle
    Vel_8 = 13.01  # The s.s. velocity(m/s) that corrresponds to 65% throttle
    Vel_9 = 15.18  # The s.s. velocity(m/s) that corrresponds to 70% throttle
    Vel_10 = 17.73  # The s.s. velocity(m/s) that corrresponds to 75% throttle

    global k_1, k_2, k_3, k_4, k_5, k_6, k_7, k_8, k_9, k_10
    # The corresponding gain for the transfer function in system identification (studied in MATLAB)

    k_1 = 11.07
    k_2 = 12.11
    k_3 = 13.40
    k_4 = 14.98
    k_5 = 16.23
    k_6 = 17.28
    k_7 = 18.43
    k_8 = 20.015
    k_9 = 21.686
    k_10 = 23.64

    global taw_1, taw_2, taw_3, taw_4, taw_5, taw_6, taw_7, taw_8, taw_9, taw_10
    # The corresponding time delay parameters for the transfer function in system identification (studied in MATLAB)

    taw_1 = 2.204
    taw_2 = 2.329
    taw_3 = 2.82
    taw_4 = 3.12
    taw_5 = 2.98
    taw_6 = 2.94
    taw_7 = 2.99
    taw_8 = 3.4776
    taw_9 = 3.9065
    taw_10 = 4.2261

    # Linear interpolation of the transfer function to have an
    # adaptive model according to the velocity of the vehicle.
    def transfer_function_interpolate(velocity):
        if velocity < Vel_1:
            K = k_1
            TAW = taw_1
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_1 <= velocity < Vel_2:
            K = k_1 + ((velocity - Vel_1) / (Vel_2 - Vel_1)) * (k_2 - k_1)
            TAW = taw_1 + ((velocity - Vel_1) / (Vel_2 - Vel_1)) * (taw_2 - taw_1)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_2 <= velocity < Vel_3:
            K = k_2 + ((velocity - Vel_2) / (Vel_3 - Vel_2)) * (k_3 - k_2)
            TAW = taw_2 + ((velocity - Vel_2) / (Vel_3 - Vel_2)) * (taw_3 - taw_2)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_3 <= velocity < Vel_4:
            K = k_3 + ((velocity - Vel_3) / (Vel_4 - Vel_3)) * (k_4 - k_3)
            TAW = taw_3 + ((velocity - Vel_3) / (Vel_4 - Vel_3)) * (taw_4 - taw_3)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_4 <= velocity < Vel_5:
            K = k_4 + ((velocity - Vel_4) / (Vel_5 - Vel_4)) * (k_5 - k_4)
            TAW = taw_4 + ((velocity - Vel_4) / (Vel_5 - Vel_4)) * (taw_5 - taw_4)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_5 <= velocity < Vel_6:
            K = k_5 + ((velocity - Vel_5) / (Vel_6 - Vel_5)) * (k_6 - k_5)
            TAW = taw_5 + ((velocity - Vel_5) / (Vel_6 - Vel_5)) * (taw_6 - taw_5)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_6 <= velocity < Vel_7:
            K = k_6 + ((velocity - Vel_6) / (Vel_7 - Vel_6)) * (k_7 - k_6)
            TAW = taw_6 + ((velocity - Vel_6) / (Vel_7 - Vel_6)) * (taw_7 - taw_6)
            K = round(K, 4)
            TAW = round(TAW, 4)


        elif Vel_7 <= velocity < Vel_8:
            K = k_7 + ((velocity - Vel_7) / (Vel_8 - Vel_7)) * (k_8 - k_7)
            TAW = taw_7 + ((velocity - Vel_7) / (Vel_8 - Vel_7)) * (taw_8 - taw_7)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_8 <= velocity < Vel_9:
            K = k_8 + ((velocity - Vel_8) / (Vel_9 - Vel_8)) * (k_9 - k_8)
            TAW = taw_8 + ((velocity - Vel_8) / (Vel_9 - Vel_8)) * (taw_9 - taw_8)
            K = round(K, 4)
            TAW = round(TAW, 4)

        elif Vel_9 <= velocity < Vel_10:
            K = k_9 + ((velocity - Vel_9) / (Vel_10 - Vel_9)) * (k_10 - k_9)
            TAW = taw_9 + ((velocity - Vel_9) / (Vel_10 - Vel_9)) * (taw_10 - taw_9)
            K = round(K, 4)
            TAW = round(TAW, 4)

        else:
            K = k_10
            TAW = taw_10
            K = round(K, 4)
            TAW = round(TAW, 4)

        return K, TAW

    # Here the transfer function of the system is converted to state-space equations.
    def state_space_param(speed):
        k, taw = transfer_function_interpolate(speed)
        sys_tf = ctrl_matlab.tf([k], [taw, 1])

        tf2ss_output = ctrl_matlab.tf2ss(sys_tf)

        A = numpy.array(tf2ss_output.A)[0][0]
        B = numpy.array(tf2ss_output.B)[0][0]
        C = numpy.array(tf2ss_output.C)[0][0]
        D = numpy.array(tf2ss_output.D)[0][0]

        AA = round(A, 4)
        BB = round(C, 4)
        CC = 1
        DD = 0

        return AA, BB

    # The controller is studied based on the target speed. So, the state-space model of the
    # target model(according to the target velocity) is considered to calculate the throttle.
    AA, BB = state_space_param(target_speed)

    # The idea behind two simulation classes is that we first calculate the throttle based on
    # the target speed and then we use the list of calculated throttle to predict the velocity
    # and the distance travelled by the vehicle in the prediction horizon.
    class simulation_ctrl(object):

        def __init__(self):
            self.insight = vehicle_longitudinal_ctrl()
            self.pid = PID(k_p, k_i, k_d, target_speed)
            self.sim = True
            self.timer = 0
            self.throttle_list = []

        def cycle(self):
            global ctrlInput, outPID, saturated_throttle

            while self.sim:

                throttle = self.pid.compute(self.insight.get_dx())
                throttle = round(throttle, 4)
                self.insight.set_ddx(throttle)
                self.insight.set_dx()
                self.throttle_list.append(throttle)
                self.timer += 1
                # time.sleep(time_step)

                if self.timer >= round(SIM_TIME / time_step):
                    self.sim = False
                    outPID = self.throttle_list
                    ctrlInput = outPID[0:round(ctrl_horizon / dt)]

    # The class PID gets 4 input arguements, i.e. the gains k_p, k_i, k_d and the target velocity
    class PID(object):

        def __init__(self, k_p, k_i, k_d, target):
            self.kp = k_p
            self.ki = k_i
            self.kd = k_d
            self.setpoint = target
            self.error = 0
            self.integral_error = 0
            self.derivative_error = 0
            self.error_last = 0
            self.output = 0
            self.init_thr = initial_thr
            self.bias = self.init_thr
            # self.bias = (
            #             self.init_thr - (target_speed - initial_vel) * k_p)  # To set the initial throttle, works for PI

        def compute(self, vel):
            self.error = self.setpoint - vel
            self.integral_error += self.error * time_step
            self.derivative_error = (self.error - self.error_last) / time_step
            self.error_last = self.error
            self.output = (self.kp * self.error) + \
                          (self.ki * self.integral_error) + \
                          (self.kd * self.derivative_error)
                          # self.bias

            if self.output >= saturated_throttle:
                self.output = saturated_throttle
            if self.output <= 0:
                self.output = 0
            return self.output

    # As mentioned before, the acceleration and velocity in this case are just used to find the throttle
    # based on the target velocity and they are not exactly the same as the true acc and vel of the vehicle.
    # Simply put, we just recreate the simulink model with selected plant, which corresponds to target velocity.
    class vehicle_longitudinal_ctrl(object):

        def __init__(self):
            self.ddx = initial_acc
            self.dx = initial_vel
            self.dxLast = float
            self.ddxLast = float

        def set_ddx(self, throttle):
            self.ddxLast = self.ddx
            self.ddx = AA * self.dx + BB * throttle

        def get_ddx(self):
            return self.ddx

        def set_dx(self):
            self.dxLast = self.dx
            self.dx += 0.5 * (self.ddxLast + self.ddx) * time_step  # trapezoidal approximation

        def get_dx(self):
            return self.dx


    def main_ctrl():
        sim = simulation_ctrl()
        sim.cycle()


    main_ctrl()

    return ctrlInput
